
Name: app-nextcloud-extension
Epoch: 1
Version: 2.1.9
Release: 2%{dist}
Summary: Nextcloud Extension - Core
License: LGPLv3
Group: ClearOS/Libraries
Source: app-nextcloud-extension-%{version}.tar.gz
Buildarch: noarch

%description
Nextcloud Extension description

%package core
Summary: Nextcloud Extension - Core
Requires: app-base-core
Requires: app-contact-extension-core
Requires: app-openldap-directory-core
Requires: app-openldap-core >= 1:2.5.2
Requires: app-users

%description core
Nextcloud Extension description

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/nextcloud_extension
cp -r * %{buildroot}/usr/clearos/apps/nextcloud_extension/

install -D -m 0644 packaging/nextcloud.php %{buildroot}/var/clearos/openldap_directory/extensions/73_nextcloud.php

%post core
logger -p local6.notice -t installer 'app-nextcloud-extension-core - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/nextcloud_extension/deploy/install ] && /usr/clearos/apps/nextcloud_extension/deploy/install
fi

[ -x /usr/clearos/apps/nextcloud_extension/deploy/upgrade ] && /usr/clearos/apps/nextcloud_extension/deploy/upgrade

exit 0

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-nextcloud-extension-core - uninstalling'
    [ -x /usr/clearos/apps/nextcloud_extension/deploy/uninstall ] && /usr/clearos/apps/nextcloud_extension/deploy/uninstall
fi

exit 0

%files core
%defattr(-,root,root)
%exclude /usr/clearos/apps/nextcloud_extension/packaging
%exclude /usr/clearos/apps/nextcloud_extension/unify.json
%dir /usr/clearos/apps/nextcloud_extension
/usr/clearos/apps/nextcloud_extension/deploy
/usr/clearos/apps/nextcloud_extension/language
/usr/clearos/apps/nextcloud_extension/libraries
/var/clearos/openldap_directory/extensions/73_nextcloud.php
