<?php

/**
 * Contact OpenLDAP user extension.
 *
 * @category   Apps
 * @package    Contact_Directory_Extension
 * @subpackage Libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/contact_extension/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('nextcloud_extension');
clearos_load_language('base');

///////////////////////////////////////////////////////////////////////////////
// C O N F I G
///////////////////////////////////////////////////////////////////////////////

$lang_gb = lang('base_gigabytes');
$lang_mb = lang('base_megabytes');

$info_map = array(
    'account_flag' => array(
        'type' => 'integer',
        'field_type' => 'list',
        'field_options' => array(
            '0' => 'Disabled',
            '1' => 'Enabled',
        ),
        'required' => FALSE,
        'validator' => 'validate_account_flag',
        'validator_class' => 'nextcloud_extension/OpenLDAP_User_Extension',
        'description' => lang('nextcloud_extension_account'),
        'object_class' => 'clearNextcloudUser',
        'attribute' => 'clearNextcloudEnabled'
    ),
    'hard_quota' => array(
        'type' => 'integer',
        'field_type' => 'list',
        'field_options' => array(
            '100 MB' => '100 ' . $lang_mb,
            '250 MB' => '250 ' . $lang_mb,
            '500 MB' => '500 ' . $lang_mb,
            '1 GB' => '1 ' . $lang_gb,
            '2 GB' => '2 ' . $lang_gb,
            '3 GB' => '3 ' . $lang_gb,
            '4 GB' => '4 ' . $lang_gb,
            '5 GB' => '5 ' . $lang_gb,
            '10 GB' => '10 ' . $lang_gb,
            '20 GB' => '20 ' . $lang_gb,
            '30 GB' => '30 ' . $lang_gb,
            '40 GB' => '40 ' . $lang_gb,
            '50 GB' => '50 ' . $lang_gb,
            '100 GB' => '100 ' . $lang_gb,
            'none' => lang('base_unlimited')
        ),
        'required' => FALSE,
        'validator' => 'validate_hard_quota',
        'validator_class' => 'nextcloud_extension/OpenLDAP_User_Extension',
        'description' => lang('nextcloud_extension_hard_quota'),
        'object_class' => 'clearNextcloudUser',
        'attribute' => 'clearNextcloudQuota'
    ),

);
