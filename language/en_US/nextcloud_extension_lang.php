<?php

$lang['nextcloud_extension_app_name'] = 'Nextcloud Extension';
$lang['nextcloud_extension_app_description'] = 'Nextcloud Extension description';
$lang['nextcloud_extension_nextcloud_accounts_extension'] = 'Nextcloud Accounts Extension';
$lang['nextcloud_extension_administrator_privileges'] = 'Administrator Privileges';
$lang['nextcloud_extension_hard_quota'] = 'Hard Quota';
$lang['nextcloud_extension_account'] = 'Account';
$lang['nextcloud_extension_account_flag_is_invalid'] = 'Account flag is invalid.';
$lang['nextcloud_extension_administrator_flag_is_invalid'] = 'Administrator flag is invalid.';
$lang['nextcloud_extension_hard_quota_is_invalid'] = 'Hard quota is invalid.';
